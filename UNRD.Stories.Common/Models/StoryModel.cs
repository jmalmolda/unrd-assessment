﻿using System;
using System.Collections.Generic;

namespace UNRD.Stories.Common.Models
{
    public class StoryModel
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
        public string Genre { get; set; }
    }
}
