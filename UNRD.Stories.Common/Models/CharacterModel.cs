﻿using System;

namespace UNRD.Stories.Common.Models
{
    public class CharacterModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}
