﻿using System;
using System.Collections.Generic;
using System.Linq;
using UNRD.Stories.DataAccess;
using UNRD.Stories.DataAccess.Entities;

namespace UNRD.Stories.IntegrationTests
{
    public static class SeedData
    {
        public static void PopulateTestData(StoriesDBContext dbContext)
        {
            var character1 = new CharacterEntity { Id = new Guid("7ae07269-c562-4bce-8779-d06d4695b143"), Name = "Character 1" };
            var character2 = new CharacterEntity { Id = new Guid("39ef085e-6e9a-4ec4-8a76-9d821b3958cb"), Name = "Character 2" };
            var character3 = new CharacterEntity { Id = new Guid("d7e38bf2-7c19-4a6b-9f1a-788fdff7ee00"), Name = "Character 3" };

            dbContext.Characters.RemoveRange(dbContext.Characters);
            dbContext.Stories.RemoveRange(dbContext.Stories);
            dbContext.SaveChanges();

            dbContext.Genres.Add(new GenreEntity { Id = new Guid() });
            dbContext.SaveChanges();

            dbContext.Stories.Add(new StoryEntity
            {
                Id = new Guid("75dbb4c4-a693-4101-8360-5c5b81573493"),
                Genre = "Genre 1",
                Title = "Title 1",
                Characters = new List<CharacterEntity>() { character1 }
            });

            dbContext.Stories.Add(new StoryEntity
            {
                Id = new Guid("2aeb8e0d-59c9-49af-b46a-dc9795ed1bb1"),
                Genre = "Genre 2",
                Title = "Title 2",
                Characters = new List<CharacterEntity>() { character2, character3 }
            });

            dbContext.Stories.Add(new StoryEntity
            {
                Id = new Guid("2aeb8e0d-59c9-49af-b46a-dc9795ed1bb2"),
                Genre = "Genre 3",
                Title = "Title 3",
            });


            dbContext.SaveChanges();
        }
    }
}
