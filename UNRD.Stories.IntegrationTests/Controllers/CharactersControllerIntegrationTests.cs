﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using UNRD.Stories.API;
using UNRD.Stories.Common.Models;
using UNRD.Stories.IntegrationTests;
using Xunit;

namespace Notifications.IntegrationTests.Controllers
{
    public class CharactersControllerIntegrationTests
    {

        public CharactersControllerIntegrationTests()
        {
        }

        [Fact]
        public async Task CanGetAllCharacters()
        {
            var factory = new CustomWebApplicationFactory<Startup>();
            var client = factory.CreateClient();
            // The endpoint or route of the controller action.
            var httpResponse = await client.GetAsync("/api/Characters");

            // Must be successful.
            httpResponse.EnsureSuccessStatusCode();

            // Deserialize and examine results.
            var stringResponse = await httpResponse.Content.ReadAsStringAsync();
            var characters = JsonConvert.DeserializeObject<IEnumerable<CharacterModel>>(stringResponse);

            // Assert the results
            Assert.Contains(characters, c => c.Name == "Character 1");
            Assert.Contains(characters, c => c.Name == "Character 2");
        }

        [Fact]
        public async Task CanGetACharacterById()
        {
            var factory = new CustomWebApplicationFactory<Startup>();
            var client = factory.CreateClient();

            var guid = "7ae07269-c562-4bce-8779-d06d4695b143";

            // The endpoint or route of the controller action.
            var httpResponse = await client.GetAsync($"/api/Characters/{guid}");

            // Must be successful.
            httpResponse.EnsureSuccessStatusCode();

            // Deserialize and examine results.
            var stringResponse = await httpResponse.Content.ReadAsStringAsync();
            var character = JsonConvert.DeserializeObject<CharacterModel>(stringResponse);

            // Assert the results
            Assert.Equal("Character 1", character.Name);
        }

        [Fact]
        public async Task CanAddACharacterToAStory()
        {
            var guid = "2aeb8e0d-59c9-49af-b46a-dc9795ed1bb2";

            var factory = new CustomWebApplicationFactory<Startup>();
            var client = factory.CreateClient();

            var content = new CharacterModel
            {
                Name = "Character 3"
            };

            // The endpoint or route of the controller action.
            var httpCreateResponse = await client.PostAsync($"/api/Stories/{guid}/Characters",
                new StringContent(
                    JsonConvert.SerializeObject(content),
                    Encoding.UTF8,
                    "application/json"));

            // Must be successful.
            httpCreateResponse.EnsureSuccessStatusCode();

            // check the character has been added
            var httpGetResponse = await client.GetAsync("/api/Characters");
            var stringResponse = await httpGetResponse.Content.ReadAsStringAsync();
            var characters = JsonConvert.DeserializeObject<IEnumerable<CharacterModel>>(stringResponse);

            // Assert the result
            Assert.Contains(characters, c => c.Name == "Character 3");
        }

        [Fact]
        public async Task CanModifyCharacter()
        {
            var factory = new CustomWebApplicationFactory<Startup>();
            var client = factory.CreateClient();

            var guid = "7ae07269-c562-4bce-8779-d06d4695b143";

            var content = new CharacterModel
            {
                Id = new Guid(guid),
                Name = "New Character"
            };

            // The endpoint or route of the controller action.
            var httpPutResponse = await client.PutAsync($"/api/Characters",
                new StringContent(
                    JsonConvert.SerializeObject(content),
                    Encoding.UTF8,
                    "application/json"));

            // Must be successful.
            httpPutResponse.EnsureSuccessStatusCode();

            // check the character has been modified
            var httpGetResponse = await client.GetAsync($"/api/Characters/{guid}");
            var stringResponse = await httpGetResponse.Content.ReadAsStringAsync();
            var character = JsonConvert.DeserializeObject<CharacterModel>(stringResponse);

            // Assert the result
            Assert.Equal("New Character", character.Name);
        }

        [Fact]
        public async Task CanDeleteCharacter()
        {
            var factory = new CustomWebApplicationFactory<Startup>();
            var client = factory.CreateClient();

            var guid = "7ae07269-c562-4bce-8779-d06d4695b143";

            // The endpoint or route of the controller action.
            var httpDeleteResponse = await client.DeleteAsync($"/api/Characters/{guid}");

            // Must be successful.
            httpDeleteResponse.EnsureSuccessStatusCode();

            // check the character has been deleted
            var httpGetResponse = await client.GetAsync($"/api/Characters");
            var stringResponse = await httpGetResponse.Content.ReadAsStringAsync();
            var characters = JsonConvert.DeserializeObject<IEnumerable<CharacterModel>>(stringResponse);

            // Assert the result
            Assert.DoesNotContain(characters, c => c.Id == new Guid(guid));
        }


    }
}
