﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using UNRD.Stories.API;
using UNRD.Stories.Common.Models;
using UNRD.Stories.IntegrationTests;
using Xunit;

namespace Notifications.IntegrationTests.Controllers
{
    public class StoriesControllerIntegrationTests
    {
        public StoriesControllerIntegrationTests()
        {
        }

        [Fact]
        public async Task CanGetAllStories()
        {
            var factory = new CustomWebApplicationFactory<Startup>();
            var client = factory.CreateClient();

            // The endpoint or route of the controller action.
            var httpResponse = await client.GetAsync("/api/Stories");

            // Must be successful.
            httpResponse.EnsureSuccessStatusCode();

            // Deserialize and examine results.
            var stringResponse = await httpResponse.Content.ReadAsStringAsync();
            var characters = JsonConvert.DeserializeObject<IEnumerable<StoryModel>>(stringResponse);

            // Assert the results
            Assert.Contains(characters, c => c.Title == "Title 1");
            Assert.Contains(characters, c => c.Title == "Title 2");
        }

        [Fact]
        public async Task CanGetAStoryById()
        {
            var factory = new CustomWebApplicationFactory<Startup>();
            var client = factory.CreateClient();

            var guid = "75dbb4c4-a693-4101-8360-5c5b81573493";

            // The endpoint or route of the controller action.
            var httpResponse = await client.GetAsync($"/api/Stories/{guid}");

            // Must be successful.
            httpResponse.EnsureSuccessStatusCode();

            // Deserialize and examine results.
            var stringResponse = await httpResponse.Content.ReadAsStringAsync();
            var story = JsonConvert.DeserializeObject<StoryModel>(stringResponse);

            // Assert the results
            Assert.Equal("Title 1", story.Title);
        }

        [Fact]
        public async Task CanCreateStory()
        {
            var factory = new CustomWebApplicationFactory<Startup>();
            var client = factory.CreateClient();

            var content = new StoryModel
            {
                Title = "new title",
                Genre = "new genre",                
            };

            // The endpoint or route of the controller action.
            var httpCreateResponse = await client.PostAsync("/api/Stories",
                new StringContent(
                    JsonConvert.SerializeObject(content),
                    Encoding.UTF8,
                    "application/json"));

            // Must be successful.
            httpCreateResponse.EnsureSuccessStatusCode();

            // check the character has been added
            var httpGetResponse = await client.GetAsync("/api/Stories");
            var stringResponse = await httpGetResponse.Content.ReadAsStringAsync();
            var stories = JsonConvert.DeserializeObject<IEnumerable<StoryModel>>(stringResponse);

            // Assert the result
            Assert.Contains(stories, s => s.Title == "new title");
        }

        [Fact]
        public async Task CanModifyStory()
        {
            var factory = new CustomWebApplicationFactory<Startup>();
            var client = factory.CreateClient();

            var guid = "75dbb4c4-a693-4101-8360-5c5b81573493";

            var content = new StoryModel
            {
                Id = new Guid(guid),
                Title = "new title",
                Genre = "new genre"
            };

            // The endpoint or route of the controller action.
            var httpPutResponse = await client.PutAsync($"/api/Stories",
                new StringContent(
                    JsonConvert.SerializeObject(content),
                    Encoding.UTF8,
                    "application/json"));

            // Must be successful.
            httpPutResponse.EnsureSuccessStatusCode();

            // check the character has been modified
            var httpGetResponse = await client.GetAsync($"/api/Stories/{guid}");
            var stringResponse = await httpGetResponse.Content.ReadAsStringAsync();
            var story = JsonConvert.DeserializeObject<StoryModel>(stringResponse);

            // Assert the result
            Assert.Equal("new title", story.Title);
        }

        [Fact]
        public async Task CanDeleteStory()
        {
            var factory = new CustomWebApplicationFactory<Startup>();
            var client = factory.CreateClient();

            var guid = "2aeb8e0d-59c9-49af-b46a-dc9795ed1bb2";

            // The endpoint or route of the controller action.
            var httpDeleteResponse = await client.DeleteAsync($"/api/Stories/{guid}");

            // Must be successful.
            httpDeleteResponse.EnsureSuccessStatusCode();

            // check the character has been deleted
            var httpGetResponse = await client.GetAsync($"/api/Stories");
            var stringResponse = await httpGetResponse.Content.ReadAsStringAsync();
            var stories = JsonConvert.DeserializeObject<IEnumerable<StoryModel>>(stringResponse);

            // Assert the result
            Assert.DoesNotContain(stories, s => s.Id == new Guid(guid));
        }


    }
}
