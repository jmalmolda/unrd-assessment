﻿using System;
using System.Collections.Generic;
using System.Linq;
using UNRD.Stories.Common.Models;
using UNRD.Stories.DataAccess;
using UNRD.Stories.DataAccess.Entities;

namespace UNRD.Stories.Services
{
    public interface IStoriesService
    {
        IReadOnlyCollection<StoryModel> Get();
        StoryModel GetById(Guid id);
        void Add(StoryModel character);
        void Update(StoryModel character);
        void Delete(Guid id);
    }

    public class StoriesService : IStoriesService
    {
        private readonly StoriesDBContext _context;

        public StoriesService(StoriesDBContext context)
        {
            _context = context;
        }

        public void Add(StoryModel story)
        {
            _context.Add(new StoryEntity
            { 
                Title = story.Title,
                Genre = story.Genre,
            });
            _context.SaveChanges();
        }

        public void Delete(Guid id)
        {
            var story = new StoryEntity { Id = id };
            _context.Remove(story);
            _context.SaveChanges();
        }

        public IReadOnlyCollection<StoryModel> Get() =>
            _context.Stories.Select(entity => new StoryModel
            {
                Id = entity.Id,
                Genre = entity.Genre,
                Title = entity.Title,
            }).ToList().AsReadOnly();


        public void Update(StoryModel story)
        {
            _context.Update(new StoryEntity
            {
                Id = story.Id,
                Title = story.Title,
                Genre = story.Genre,
            });
            _context.SaveChanges();
        }


        public StoryModel GetById(Guid id)
        {
            var entity = _context.Stories.FirstOrDefault(story => story.Id == id);
            if (entity == null)
            {
                return null;
            }
            return new StoryModel
            {
                Id = entity.Id,
                Title = entity.Title,
                Genre = entity.Genre,
            };
        }
    }
}
