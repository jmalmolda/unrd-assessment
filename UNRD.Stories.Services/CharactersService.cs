﻿using System;
using System.Collections.Generic;
using System.Linq;
using UNRD.Stories.Common.Models;
using UNRD.Stories.DataAccess;
using UNRD.Stories.DataAccess.Entities;

namespace UNRD.Stories.Services
{
    public interface ICharactersService
    {
        IReadOnlyCollection<CharacterModel> Get();
        CharacterModel GetById(Guid id);
        void Add(Guid storyId, CharacterModel character);
        void Update(CharacterModel character);
        void Delete(Guid id);
    }

    public class CharactersService : ICharactersService
    {
        private readonly StoriesDBContext _context;

        public CharactersService(StoriesDBContext context)
        {
            _context = context;
        }

        public IReadOnlyCollection<CharacterModel> Get() => 
            _context.Characters.Select(entity => new CharacterModel
                {
                    Id = entity.Id,
                    Name = entity.Name
                }).ToList().AsReadOnly();

        public CharacterModel GetById(Guid id)
        {
            var entity = _context.Characters.FirstOrDefault(entity => entity.Id == id);
            if (entity == null)
            {
                return null;
            }
            return new CharacterModel { Id = entity.Id, Name = entity.Name };
        }

        public void Add(Guid storyId, CharacterModel character)
        {
            var characterEntity = new CharacterEntity { Name = character.Name, Id = Guid.NewGuid() };
            _context.Characters.Add(characterEntity);
            _context.SaveChanges();


            var story = _context.Stories.FirstOrDefault(story => story.Id == storyId);
            if (story == null)
            {
                throw new Exception("A Character needs to be added to an existing story");
            }
            var ch = _context.Characters.Find(characterEntity.Id);


            story.Characters.Add(ch);
            _context.SaveChanges();
        }

        public void Update(CharacterModel character)
        {
            _context.Update(new CharacterEntity { Id = character.Id, Name = character.Name });
            _context.SaveChanges();
        }

        public void Delete(Guid id)
        {
            var character = new CharacterEntity { Id = id };
            _context.Remove(character);
            _context.SaveChanges();
        }
    }
}
