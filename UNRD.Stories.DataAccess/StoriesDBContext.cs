﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using UNRD.Stories.DataAccess.Entities;

namespace UNRD.Stories.DataAccess
{
    public class StoriesDBContext : DbContext
    {
        public StoriesDBContext(DbContextOptions<StoriesDBContext> options)
           : base(options)
        { }
        public DbSet<GenreEntity> Genres { get; set; }
        public DbSet<CharacterEntity> Characters { get; set; }
        public DbSet<StoryEntity> Stories { get; set; }
        protected override void OnConfiguring(DbContextOptionsBuilder options)
            => options.UseSqlite("Data Source=unrd.stories.db");
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<GenreEntity>();
            modelBuilder.Entity<CharacterEntity>();
            modelBuilder.Entity<StoryEntity>();
        }
    }
}
