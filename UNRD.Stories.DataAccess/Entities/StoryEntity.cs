﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UNRD.Stories.DataAccess.Entities
{
    public class StoryEntity
    {
        public Guid Id { get; set; } = Guid.NewGuid();
        public string Title { get; set; }
        public string Genre { get; set; }
        public List<CharacterEntity> Characters { get; set; } = new List<CharacterEntity>();
    }
}
