﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UNRD.Stories.DataAccess.Entities
{
    public class GenreEntity
    {
        public Guid Id { get; set; } = Guid.NewGuid();
        public string Name { get; set; }
    }
}
