﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using UNRD.Stories.Common.Models;
using UNRD.Stories.Services;

namespace UNRD.Stories.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StoriesController : ControllerBase
    {
        private readonly IStoriesService _storiesService;

        public StoriesController(IStoriesService storiesService)
        {
            this._storiesService = storiesService;
        }

        // GET: api/Stories
        [HttpGet]
        [EnableCors("Allow")]
        public IReadOnlyCollection<StoryModel> Get()
        {
            return _storiesService.Get();
        }

        // GET: api/Stories/e8ef9a3e-49e7-45d6-a3de-fc7c1072a980
        [HttpGet("{id}", Name = "GetStories")]
        [EnableCors("Allow")]
        public StoryModel Get(Guid id)
        {
            return _storiesService.GetById(id);
        }

        // POST: api/stories
        [HttpPost]
        [EnableCors("Allow")]
        public void Post([FromBody] StoryModel model)
        {
            _storiesService.Add(model);
        }

        // PUT: api/Stories
        [HttpPut]
        [EnableCors("Allow")]
        public void Put([FromBody] StoryModel model)
        {
            _storiesService.Update(model);
        }

        // DELETE: api/Stories/e8ef9a3e-49e7-45d6-a3de-fc7c1072a980
        [HttpDelete("{id}")]
        [EnableCors("Allow")]
        public void Delete(Guid id)
        {
            _storiesService.Delete(id);
        }
    }
}
