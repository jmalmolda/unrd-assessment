﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using UNRD.Stories.Common.Models;
using UNRD.Stories.Services;

namespace UNRD.Stories.API.Controllers
{
    [ApiController]
    [Route("api/")]
    public class CharactersController : ControllerBase
    {
        private readonly ICharactersService _charactersService;

        public CharactersController(ICharactersService charactersService)
        {
            _charactersService = charactersService;
        }

        // GET: api/Characters
        [HttpGet("Characters")]
        public IReadOnlyCollection<CharacterModel> Get()
        {
            return _charactersService.Get();    
        }

        // GET: api/Characters/e8ef9a3e-49e7-45d6-a3de-fc7c1072a980
        [HttpGet("Characters/{id}", Name = "GetCharacters")]
        public CharacterModel Get(Guid id)
        {
            return _charactersService.GetById(id);
        }

        // POST: api/Stories/e8ef9a3e-49e7-45d6-a3de-fc7c1072a980/Characters
        [HttpPost("Stories/{storyId}/Characters")]
        public void Post(Guid storyId, [FromBody] CharacterModel model)
        {
            _charactersService.Add(storyId, model);
        }

        // PUT: api/Characters
        [HttpPut("Characters")]
        public void Put([FromBody] CharacterModel model)
        {
            _charactersService.Update(model);
        }

        // DELETE: api/Characters/e8ef9a3e-49e7-45d6-a3de-fc7c1072a980
        [HttpDelete("Characters/{id}")]
        public void Delete(Guid id)
        {
            _charactersService.Delete(id);
        }
    }
}
